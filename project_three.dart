void main() {
  var winner = WinningApp();
  winner.name = "Ambani-Afrika";
  winner.category = "Best Educational solution";
  winner.developer = "Mukundi Lambani";
  winner.year = 2021;

  print(winner.name);
  print(winner.category);
  print(winner.developer);
  print(winner.year);
  winner.printINcaps();
}

class WinningApp {
  String? name;
  String? category;
  String? developer;
  int? year;

  void printINcaps() {
    print("$name".toUpperCase());
  }
}
